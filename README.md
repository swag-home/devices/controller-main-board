# 🎮 Controller Main Board

Documentation of the controller main board, used to provide a simplified and servicable board to assemble devices

![3D Render](https://gitlab.com/swag-home/devices/controller-main-board/-/raw/main/images/3d_view.PNG)

## Goals of this project

This project is the central controller board, aimed to be as minimalistic as possible to be compatible with as any microcontroller as possible (from 8-pin MCU or boards like the ESP8266-01 to bigger ones).

The extra pins or features of the controller plugged on the board should be exposed on the controller itself, though it's better if it's not required for the device to work or it would reduce the compatibility and thus choice of available controllers.

The pinout of the controller to plug on the board is inspired from the ESP8266-01 as its minimalistic, cheap and still available even with components shortages.

Also the chosen serial bus is I2C, not UART, as it's commonly found on many controllers and is able to manage by design multiple slaves devices on the same bus while a device can act either as a master or device and still requiring only 2 wires.

The board itself is designed in a manner to accept all components to be in Through-Hole format while further revisions will try to provide universally compatible SMD footprints so the board should be servicable with any components, tools and skill levels available.

Because of this, the board is also designed to use components as widely available as possible so that they could also possibly be recycled from older devices and for more complex components to provide instead footprints to assemble them, so any compatible one respecting the footprint (or newly designed one) could be used instead of focusing on space saving.

## Description of the board

The schematic of the board is available here:

![Schematic](https://gitlab.com/swag-home/devices/controller-main-board/-/raw/main/schematics/schematic_swaghome_controller_board.svg)

The board is composed of the following sections shown on the schematic:

- U1: `ESP8266`: The controller used with the board (ESP8266 is recommended for minimal support of required features but can be replaced with adapter / dedicated boards by other controllers such as an ESP32 or NRF52)
- H1: `PWR`: The main alimentation header to supply power to the board. If the power supply is already regulated to the right voltage `U2` is optional and can be replaced by direct wire connections
- H2: `DBG`: Reserved pin for the controller to print debug information if needed
- H3: `1W`: Header for a GPIO for usages requiring only 1 wire (for example a single ADC input, a PWM output, toggling a relay or MOSFET, ...etc)
- H4: `I2C`: Header for the I2C bus of the controller
- H5: `BKP`: Header for the backup power supply (for example a backup battery if not already from the power supply or a solar panel). It's optional however if used, the associated diode is required to prevent current flowing back to the backup power supply when the main power supply is active
- U2: Footprint of the `MINI-360` step-down DC/DC regulator to convert the voltage of the power supply to fixed value if it's unregulated. It can be replaced by direct wire connection if the input power supply of the board doesn't need any regulation.

## Preview of the board

Simulated renders of the board are visible here:

![PCB Top](https://gitlab.com/swag-home/devices/controller-main-board/-/raw/main/images/top.PNG)
![PCB Bottom](https://gitlab.com/swag-home/devices/controller-main-board/-/raw/main/images/bottom.PNG)

## Building the board

Design file of the PCB and typons are available in the [pcb folder](https://gitlab.com/swag-home/devices/controller-main-board/-/tree/pcb)
